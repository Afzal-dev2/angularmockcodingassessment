import { Component } from '@angular/core';

@Component({
  selector: 'app-main-news',
  templateUrl: './main-news.component.html',
  styleUrl: './main-news.component.css'
})
export class MainNewsComponent {
  mainNews: { title: string, imageUrl:String, content: string, author: string } = {
    title: "The 600,000 IT job shortage in India and how to solve it",
    imageUrl: "https://images.unsplash.com/photo-1496347326319-2935d381b307?q=80&w=2833&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    content: `
    The job market has never been a straightforward path. Ask anyone who has ever looked for a job, certainly within the last decade, and they can tell you as much. But with the rapid development of AI and machine learning, concerns are growing for people about their career options, with a report from Randstad finding that 7 in 10 people in India are concerned about their job being eliminated by AI.

    Employers have their own share of concerns. According to The World Economic Forum, 97 million new AI-related jobs will be created by 2025 and the share of jobs requiring AI skills will increase by 58%. The IT industry in India is experiencing a tremendous surge in demand for skilled professionals on disruptive technologies like artificial intelligence, machine learning, blockchain, cybersecurity and, according to Nasscom, this is leading to a shortage of 600,000 profiles.  So how do we fill those gaps? Can we democratize access to top-tier higher education in technology?.`,
    author: 'Scott Brownlee'
  };

  constructor() { }
}
