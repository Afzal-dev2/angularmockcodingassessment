import { Component } from '@angular/core';

@Component({
  selector: 'app-news-headings',
  templateUrl: './news-headings.component.html',
  styleUrl: './news-headings.component.css'
})
export class NewsHeadingsComponent {
  newsList: { title: string, author: string }[] = [
    { title: "Madhav Sheth Beyond connectivity: India’s telecom sector envisions a data-driven future", author: 'John Doe' },
    { title: "The Indian AI paradox: Managing innovation and regulation in AI Governance", author: 'Jane Smith' },
    { title: "A lot of guys with out-and-out raw pace don’t have control, Mayank Yadav looks to have both: Tim Southee", author: 'Michael Johnson' },
    {
      title: "Why aren’t you afraid of posting your pictures online anymore?",
      author: 'William Anderson'
    },
    {
      title: "Exclusive | ‘Aadujeevitham’ director Blessy likely to stage protest as PVR Cinemas halt screening of Malayalam movies",
      author: 'Adam Doe'
    },
    {
      title: "Lok Sabha election LIVE Updates | Congress to conduct caste census if voted to power at Centre: Rahul Gandhi",
      author: 'Eve Smith'
    }
  ];
}
